$ ->
  if $("#tree_vue").is("*")
    treeVue = new Vue
      el: "#tree_vue"
      data:
        treeData: gon.tree
        currentNode: gon.current_node
        newFolderLabel: "" # 新規作成フォルダ名
        styleObject:
          cursor: "pointer"
      # DOMに追加されたタイミングで発動
      attached: ->
        @jqtree = $("#tree")
        @jqtree.tree
          data: @treeData
          autoOpen: true
        @jqtree.tree('selectNode', @jqtree.tree('getNodeById', @currentNode))
        # jqtreeのイベントを記述
        @jqtree.bind 'tree.click', (event)=>
          @currentNode = event.node.id
          @requestNode()
      # データの監視
      watch:
        treeData: (val)->
          @jqtree.tree("loadData", val)
        currentNode: (val)->
          @jqtree.tree('selectNode', @jqtree.tree('getNodeById', val))
      methods:
        # @currentNodeに対応する一覧を取得
        requestNode: ->
          $.pjax
            container: '#data_ajax'
            type: 'GET'
            url: Routes.list_boxes_path()
            data:
              node_id: @currentNode
        # フォルダの追加
        addFolder: ->
          $.ajax
            type: 'POST'
            url: Routes.folders_create_path()
            data:
              node_id: @currentNode
              folder_label: @newFolderLabel
            complete: (result) =>
              @treeData = result.responseJSON.folders
              @currentNode = result.responseJSON.current_node_id
              @newFolderLabel = ""
              @requestNode()

        # Railsのnew画面をjsで開く
        createBox: ->
          window.location.href = Routes.new_box_path(node_id: @currentNode)

    # 戻るボタン押下時に書き換えられるURLを取得してjqtreeの選択ノードを強制的に更新
    $(window).on 'popstate', (event) ->
      state = event.originalEvent.state
      node_id = $.url(state.url).param("node_id")
      treeVue.currentNode = node_id
