class FoldersController < ApplicationController
  def create
    parent = Folder.find_by_id(params[:node_id])
    folder = Folder.create(label: params[:folder_label], parent: parent)
    render json: {current_node_id: folder.id, folders: Folder.all.arrange_serializable}
  end
end